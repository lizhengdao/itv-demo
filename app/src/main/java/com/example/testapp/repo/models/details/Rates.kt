package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Rates(
    @SerializedName("imdb")
    val imdb: Any?, // null
    @SerializedName("kinopoisk")
    val kinopoisk: Any?, // null
    @SerializedName("itv")
    val itv: Any? // null
)