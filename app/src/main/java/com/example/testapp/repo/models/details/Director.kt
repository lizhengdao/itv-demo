package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Director(
    @SerializedName("id")
    val id: Int?, // 17496
    @SerializedName("name")
    val name: String?, // Габриэле Муччино
    @SerializedName("photo_url")
    val photoUrl: String? // https://files.itv.uz/uploads/people/2019/08/26//60f2e95afaa479be73347310d64e8177-q-170x225.jpeg
)