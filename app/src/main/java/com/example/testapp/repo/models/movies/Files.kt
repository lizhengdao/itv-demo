package com.example.testapp.repo.models.movies


import com.google.gson.annotations.SerializedName

data class Files(
    @SerializedName("poster_url")
    val posterUrl: String? // https://files.itv.uz/uploads/content/poster/2020/11/04/b7738d1d2095dc9d6af54738421dadf8-q-350x501.jpeg
)