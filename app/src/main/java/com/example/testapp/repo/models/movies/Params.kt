package com.example.testapp.repo.models.movies


import com.google.gson.annotations.SerializedName

data class Params(
    @SerializedName("is_hd")
    val isHd: Boolean?, // true
    @SerializedName("is_3d")
    val is3d: Boolean?, // false
    @SerializedName("is_4k")
    val is4k: Boolean?, // false
    @SerializedName("is_new")
    val isNew: Boolean?, // true
    @SerializedName("is_free")
    val isFree: Boolean?, // false
    @SerializedName("is_tvshow")
    val isTvshow: Boolean? // false
)