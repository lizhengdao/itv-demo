package com.example.testapp.repo.models.details

import com.google.gson.annotations.SerializedName

data class Snapshots(
    @SerializedName("snapshot_url")
    val snapshots: String?
)