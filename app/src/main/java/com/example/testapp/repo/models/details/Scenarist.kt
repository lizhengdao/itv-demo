package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Scenarist(
    @SerializedName("id")
    val id: Int?, // 27111
    @SerializedName("name")
    val name: String?, // Робби Фокс
    @SerializedName("photo_url")
    val photoUrl: Any? // null
)