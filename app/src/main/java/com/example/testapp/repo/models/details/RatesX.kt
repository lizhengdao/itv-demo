package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class RatesX(
    @SerializedName("imdb")
    val imdb: Double?, // 5.9
    @SerializedName("kinopoisk")
    val kinopoisk: Double?, // 6.624
    @SerializedName("itv")
    val itv: Any? // null
)